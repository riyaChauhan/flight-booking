<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
  <!DOCTYPE html>
  <html>

  <head>
    <meta charset="ISO-8859-1">
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- font -->
    <link href="https://fonts.googleapis.com/css?family=Nunito&display=swap" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css" media="screen,projection" />
    <link rel="stylesheet" type="text/css" href="css/loginCSS.css">
    <link rel="stylesheet" href="css/footerstyle.css">

    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>

    <title>Login</title>
  </head>

  <body>
    <%@page import="java.util.ArrayList"%>      
    <%@page import="com.wipro.fms.DAO.UserDao"%>
    <%@page import="com.wipro.fms.bean.UserBean"%>
    <%@page import="java.util.Iterator"%>
    <style>
      body {
        display: flex;
        min-height: 100vh;
        flex-direction: column;
        font-family: 'Nunito' !important; 
        background-image: url('images/bookingCardImg1.jpg');
      }

      main {
        flex: 1 0 auto;
      }

      .bg_trans{background-color:transparent !important;} 
      .bg021{background-color: #02122c !important;}
      .p030{padding: 0 30px;}
      .dIB{display: inline-block;}
      .w250{width: 250px !important;}
      .fs20{font-size: 20px !important;}
      .pswdEye{
        margin-top: -33px;
        color: rgba(0,0,0,0.7);
        margin-right: 10px;
        cursor:pointer;
      }
    </style> 

    <!-- nav bar -->
    <nav>
      <div class="nav-wrapper bg021">
        <div class="container">
            <a href="#!" class="brand-logo " style="font-family: 'Pacifico', cursive;"><b>musafir.com</b></a>
        </div>
        <a href="#" data-target="side" class="sidenav-trigger  navy-color lighten-2"><b><i class="material-icons">menu</i></b></a>
        <ul class="right hide-on-med-and-down">
            <li>
              <a class="btn bg_trans lighten-2  waves-effect "> <b>About</b></a>
            </li>
            <li>
              <a data-target="modal1" class=" btn  bg_trans lighten-2  waves-effect modal-trigger"> <b>User LogIn</b></a>\
            </li>
            <li>
              <a data-target="modal3" class="btn bg_trans lighten-2 modal-trigger waves-effect"><b>Register</b></a>
            </li> 
        </ul>
      </div>
    </nav>

    <!-- sidenav -->
    <div class="sidenav" id="side">
      <div id="uli" class="container">
        <ul>
          <li>
            <a class="white-text sidenav-close">About</a>
          </li>
          <li>
            <a data-target="modal1" class="modal-trigger white-text sidenav-close">LogIn</a>
          </li>
          <li>
            <a data-target="modal3" class="white-text sidenav-close modal-trigger">Register</a>
          </li>
        </ul>
      </div>
      <footer id="sideFooter" class="footer-copyright ">
        <div>
          <p>FOLLOW US</p>
          <a class="sidenav-close">
            <img src="icons/facebook.png">
          </a>
          <a class=" sidenav-close">
            <img src="icons/twitter.png">
          </a>
          <a class=" sidenav-close">
            <img src="icons/instagram.png">
          </a>
          <a class=" sidenav-close">
            <img src="icons/google-plus.png">
          </a>
          <div class="grey-text">
            <p>ÃÂ©2019 Copyright</p>
          </div>
        </div>
      </footer>
    </div>

    <!-- s i d e n a v e n d -->


    <!-- My account Dropdown Structure -->
    <ul id='acc-drop1' class='dropdown-content'>
      <li><a href="#!">one</a></li>
      <li><a href="#!">two</a></li>
      <li class="divider" tabindex="-1"></li>
      <li><a href="#!">three</a></li>
      <li><a href="#!"><i class="material-icons">view_module</i>four</a></li>
      <li><a href="#!"><i class="material-icons">cloud</i>five</a></li>
    </ul>

    <!-- U s e r l o g i n   f o r m s  -->

    <!-- User Log In Modal Structure -->
    <div id="modal1" class="modal">
      <div class="modal-content">
        <!-- LOGIN Page -->
        <div class="">
            <div class="grey lighten-4 row" style="display: inline-block; padding: 30px 30px 0px 30px;">
              <form class="col s12" action="UserLoginServlet" method="post" >
                <!-- <div class='row'>
                    <div class='col s12'>
                    </div>
                </div> -->

                <!-- heading -->
                <div class="imgal">
                    <img class="responsive-img" style="width: 250px; " src="images/login_img.png" alt="login" />
                    <p style="text-align: center;font-size: 25px;"><b>User Log In</b></p>
                </div>
                
                <!-- email -->
                <div class='row'>
                    <div class='input-field col s12'>
                        <input class='validate' type='email' name='email' id='email' />
                        <label for='email'>Enter your email</label>
                    </div>
                </div>

                <!-- password -->
                <div class='row'>
                  <div class='input-field col s12'>
                    <input class='validate' type='password' name='password' id='password' />
                    <label for='password'>Enter your password</label><br>
                  </div>
                  <label id="to2">
                    <span>
                      <input type="checkbox" class="filled-in" onclick='showpassword()' />
                    </span>&nbsp;
                    <span>Show password</span>
                  </label>
                  <label style='float: right;'>
                    <a data-target="modal2" class="modal-trigger modal-close" Style="color:#d81b60;">Forget Password?</a>
                  </label>
                </div>

                <div class='row' >
                  <button type='submit' value="LOGIN" name='btn_login' class='col s4 offset-s4 btn btn-medium login-btn waves-effect indigo'>Login</button>
                </div>

                <div class='row' style="text-align: center;font-size: 13px;">
                  <em>New User?</em>
                  <a data-target="modal3" class="modal-trigger modal-close">Create account</a>
                </div>
              </form>
            </div>
        </div>
      </div>
    </div>


    <!--  F O R G E T     P A S S W O R D -->


    <!-- Modal Structure -->
    <div id="modal2" class="modal">
        <div class="modal-content">
            <!-- ForgetPassword Page -->
            <div class="box1" style=" text-align: center;">
                <div class="grey lighten-4 row" style="display: inline-block; padding: 20px;width:100%;">
                    <div class='row' style="text-align: left;">
                        <a data-target="modal1" class="modal-trigger modal-close circle" style="color: #02122C;"><i class="material-icons ">arrow_back</i></a>
                    </div>
                    <form class="col s12" method="post">
                        <div class='row'>
                            <div class='col s12'>
                            </div>
                        </div>

                        <div class="imgal1">
                            <img class="responsive-img" id="imgal2" src="images/lock1.png" alt="login" />

                            <p style="text-align: center;font-size: 25px;"><b>Forgot Password</b></p>
                        </div>
                        <!--<h3 class="blue-text" align="center"><b>Log In</b></h3>-->
                        <div class='row'>
                            <div class='input-field col s12'>
                                <input class='validate' type='email' name='email' id='email' />
                                <label for='email'>Enter your email</label>
                            </div>
                        </div>
                        <br />
                        <div class='row'>
                            <button type='submit' value="login" name='btn_login' class='col s6 offset-s3 btn btn-medium login-btn waves-effect indigo'>Send Link</button>
                            <br>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>

    <!--  N E W    U S E R  -->

    <div id="modal3" class="modal">
      <div class="modal-content">
        <!-- LOGIN Page -->
        <div class="">
          <div class="grey lighten-4 row p030 dIB">
            <form class="col s12" action="UserLoginServlet" method="post">
              <!-- <div class='row'>
                <div class='col s12'>
                </div>
              </div> -->

              <!-- Create Account -->
              <div class="imgal">
                <img class="responsive-img w250" src="images/signup.png" alt="login" />
                <p class="center-align fs20"><b>Create Account</b></p>
              </div>

              <!-- Name -->
              <div class='row'>
                <div class='input-field col s6'>
                  <input class='validate' name='firstname' id='first' type='text' />
                  <label for='first'>First Name</label>
                </div>
                <div class='input-field col s6'>
                  <input type='text' name='lastname' id='last'>
                  <label for='last'>Last Name</label>
                </div>
              </div>

              <!-- Phone Number -->
              <div class='row'>
                <div class='input-field col s12'>
                  <input class='validate' type='text' name='phonenumber' id='phonenumber'>
                  <label for='phonenumber'>Enter Your Phone Number</label>
                </div>
              </div>

              <!-- Email -->
              <div class='row'>
                <div class='input-field col s12'>
                  <input class='validate' type='email' name='email' id='email' />
                  <label for='email'>Enter your Email Id</label>
                </div>
              </div>

              <!-- password -->
              <div class='row'>
                <div class='input-field col s11'>
                  <input class='validate' type='password' name='password' id='password' />
                  <label for='password'>Enter your password</label>
                </div>
                <i class="material-icons right eyesignup pswdEye" onclick="showpassword()">remove_red_eye</i>
              </div>

              <!-- getting status of login from params -->
              <%String s1=request.getParameter("status"); %>
              <!-- register button -->
              <div class='row'>
                <button type='submit' value="REGISTER" name='btn_login' class='col s4 offset-s4 btn btn-medium login-btn waves-effect indigo' onclick="M.toast({html: '<%=s1%>'})">Register</button>
              </div>

              <!-- existing user -->
              <div class='row center-align' style="font-size: 13px;">
                <em>Already have an account?</em>
                <a data-target="modal1" class="modal-trigger modal-close">LogIn</a>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
       
    <!-- l o g i n  f o r m s  e n d s -->
            

    <!--f o o t e r   s t a r t-->

    <footer class="footer-distributed" style="margin-top: 10%">
      <div class="footer-left">
        <h3><a href="#!" class="brand-logo white-text" style="font-family: 'Pacifico', cursive;"><b>musafir.com</b></a></h3>
        <p class="footer-links">
          <a href="#" class="link-1">Home</a>
          <a href="#">About</a>
          <a href="#">FAQ</a>
        </p>
        <p class="footer-company-name">
          <i>Musafir <i class="tiny material-icons">copyright</i> 2019</i>
        </p>
      </div>

      <div class="footer-center">
        <div>
          <i class="medium material-icons">location_on</i>
          <p><span>MUSAFIR Limited</span>Electronic city, bengaluru-560100</p>
        </div>

        <div>
          <i class="medium material-icons">call</i>
          <p>+91-7987371529</p>
        </div>

        <div>
          <i class="medium material-icons">mail</i>
          <p><a href="mailto:support@musafir.com">support@musafir.com</a></p>
        </div>
      </div>

      <div class="footer-right">
        <p class="footer-company-about">
          <span>About the company</span>
          Lorem ipsum dolor sit amet, consectateur adispicing elit. Fusce euismod convallis velit, eu auctor lacus vehicula sit amet.
        </p>

        <div class="footer-icons">
          <a href="#"><i class="fa fa-facebook"><img src="icons/facebook-3-32.png"></i></a>
          <a href="#"><i class="fa fa-twitter"><img src="icons/twitter-3-32.png"></i></a>
          <a href="#"><i class="fa fa-linkedin"><img src="icons/linkedin-3-32.png"></i></a>
          <a href="#"><i class="fa fa-github"><img src="icons/github-3-32.png"></i></a>
        </div>
      </div>
    </footer>

    <!--f o o t e r  e n d s-->
    
    <!--JavaScript at end of body for optimized loading-->
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <script type="text/javascript">
      $(document).ready(function() {
        $('.sidenav').sidenav();
      });

      $('.dropdown-trigger').dropdown();

      $(document).ready(function() {
        $('.modal').modal();
      });

      // login show pass
      $(".toggle-password").click(function() {
        $(this).toggleClass("");
        var input = $($(this).attr("toggle"));
        if (input.attr("type") == "password") {
          input.attr("type", "text");
        } else {
          input.attr("type", "password");
        }
      });
      $(document).ready(function() {
        $('.tooltipped').tooltip();
      });
    </script>

    <!-- Script for login -->
    <script>
      function showpassword() {
        var x = document.getElementById("password");
        if (x.type === 'password') {
            x.type = 'text';
        } else {
            x.type = 'password';
        }
      }
    </script>

    <!-- jquery initialization -->
    <script type="text/javascript">
      $(document).ready(function() {
          $('.modal').modal();
      });
    </script>

  </body>    
</html>


