<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">

	<title>Available Flights</title>
	<!--Import Google Icon Font-->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

	<!-- Compiled and minified CSS -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
	<!--Let browser know website is optimized for mobile-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
	<style type="text/css">
		/*genral classes*/
		.p0{padding: 0px !important;}
		.p14{padding: 14px !important;}
		.m0{margin:0px !important;}
		.mb20{margin-bottom: 20px !important;}
		.pointCursor{cursor: pointer !important;}
		.ca7{color: #a7a3a2;}
		.c31c{color: #31cac8 !important;}
		.vaB{vertical-align: bottom;}
		.br6{border-radius: 6px !important;}
		.fs82rem{font-size: 0.82rem;}

		.cardBoxshadow{
			box-shadow:1px 1px 4px 0px #aaaaac !important;
		}
		.d0e{background-color: #d0eeec !important;}
		.p14_2rem{padding: 14px 2rem !important;}
		.wp92{width: 92% !important;}


		/*overrided class*/
		.collapsible-header{border-bottom: none !important;}



		/*custom css*/
		.flightDuration{
			border-bottom: 2px solid #6a6a74;
			width: 100px;
			padding-bottom: 2px;
			font-size: 0.9rem;
		}
		.flightDuration:after{
			position: relative;
		    top: 10px;
		    right: -102px;
		    content: "";
		    display: block;
		    width: 1.2rem;
		    height: 1rem;
		    margin-top: -.32rem;
		    background-color: #fafafa;
		    background-image: url(https://js.skyscnr.com/sttc/acorn/static/media/plane-end.c2f1764f.svg);
		    background-repeat: no-repeat;
		    background-position: 100% 50%;
		    background-size: 1rem;
		}
		.flightType{
			padding-top: 4px;
		    font-size: 0.8rem;
		    letter-spacing: 0.4px;
		    color: #31aca8;
		}

		.flightCardP2{
			border-left: 1px solid #dddde5;
			padding: 26px;
			text-align: center;
		}

		.flightPrice{
			font-size: 1.6rem;font-weight: 700;
		}
		.learnMorebtn{
			font-weight: 800;
		    padding: 6px 0px;
		    color: #31aca8;
		    font-size: 0.94rem;
		}
	</style>
</head>
<body>
	<div class="container">

		<!-- for single trip -->
		<div class="row">
			<div class="col s10 offset-s1 mb20">
				<ul class="collapsible m0 d0e br6">
				    <li>
						<div class="collapsible-header d0e">
							<i class="material-icons">filter_drama</i>
							<div class="wp92">
								<b>Greener choice</b><br>
								<span class="fs82rem">This flight emits <b>12% less CO₂</b> than the average for your search</span>
							</div>
							<i class="material-icons m0 c31c">info</i>
						</div>
				      	<div class="collapsible-body p14_2rem">
					      	<div class="fs82rem" style="display: inline-block;width: 70%;vertical-align: top;">Greener flights are calculated based on <b>aircraft type, capacity</b> and <b>number of stops</b>.<div class="learnMorebtn">Learn More<i class="material-icons" style="vertical-align: middle;">arrow_forward</i></div></div>
					      	<div style="display: inline-block;width: 26%;text-align: right;"><img src="https://js.skyscnr.com/sttc/acorn/static/media/ecoCalculation.17f78182.svg" width="110px"></div>
				      	</div>
				    </li>
				</ul>
				<div class="col s12 card p0 br6 cardBoxshadow m0" style="border: 1px solid #dddde5;">
					<div class="col s12 m8 p0 pointCursor">
						
						<div class="row m0">
							<div class="col s12" style="padding-top: 30px;">
								<div class="col s12 m3">
									<img src="https://www.skyscanner.net/images/airlines/small/49.png">
								</div>
								<div class="col s12 m9">
									<div style="display: flex;">
										<div style="padding: 0px 12px;"><span style="font-size: 1.4rem;">23:03</span><br>BLR</div>
										<div style="padding: 0px 12px; text-align: center;">
											<div class="flightDuration">2h 40</div>
											<div class="flightType">Direct</div>
										</div>
										<div style="padding: 0px 12px;"><span style="font-size: 1.4rem;">23:03</span><br>BLR</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="col s12 m4 p0 pointCursor">
						<!-- <div style="width: 14px;
					    height: 14px;
					    border-radius: 50%;
					    position: absolute;
					    top: -8px;
					    right: 244px;
					    background: white;
					    box-shadow: inset 0px -1px 0px 0px rgba(37,32,3,0.2)"></div> -->
					    
						<!-- <div style="width: 14px;
					    height: 14px;
					    border-radius: 50%;
					    position: absolute;
					    right: 244.5px;
					    bottom: -10px;
					    background: white;
					    box-shadow: inset 0px 2px 0px 0px rgba(37,32,3,0.2)"></div> -->
						<div class="flightCardP2 p14">
							<div class="ca7">5 deals from</div>
							<div class="flightPrice">&#x20b9; 10,582</div>
							<div class="btn">Select <i class="material-icons vaB">arrow_forward</i></div>
						</div>
					</div>
				</div>
			</div>
			
			<!-- for round trip -->
			<div class="col s10 offset-s1 mb20">
				<ul class="collapsible m0 d0e br6">
				    <li>
						<div class="collapsible-header d0e">
							<i class="material-icons">filter_drama</i>
							<div class="wp92">
								<b>Greener choice</b><br>
								<span class="fs82rem">This flight emits <b>12% less CO₂</b> than the average for your search</span>
							</div>
							<i class="material-icons m0 c31c">info</i>
						</div>
				      	<div class="collapsible-body p14_2rem">
					      	<div class="fs82rem" style="display: inline-block;width: 70%;vertical-align: top;">Greener flights are calculated based on <b>aircraft type, capacity</b> and <b>number of stops</b>.<div class="learnMorebtn">Learn More<i class="material-icons" style="vertical-align: middle;">arrow_forward</i></div></div>
					      	<div style="display: inline-block;width: 26%;text-align: right;"><img src="https://js.skyscnr.com/sttc/acorn/static/media/ecoCalculation.17f78182.svg" width="110px"></div>
				      	</div>
				    </li>
				</ul>
				<div class="col s12 card p0 br6 cardBoxshadow m0" style="border: 1px solid #dddde5;">
					<div class="col s12 m8 p0 pointCursor">
						<div class="row m0">
							<div class="col s12" style="padding-top: 18px;">
								<div class="col s12 m3">
									<img src="https://www.skyscanner.net/images/airlines/small/0S.png">
								</div>
								<div class="col s12 m9">
									<div style="display: flex;">
										<div style="padding: 0px 12px;"><span style="font-size: 1.4rem;">23:03</span><br>BLR</div>
										<div style="padding: 0px 12px; text-align: center;">
											<div class="flightDuration">2h 40</div>
											<div class="flightType">Direct</div>
										</div>
										<div style="padding: 0px 12px;"><span style="font-size: 1.4rem;">23:03</span><br>BLR</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row m0">
							<div class="col s12" style="padding-top: 18px; padding-bottom: 8px;">
								<div class="col s12 m3">
									<img src="https://www.skyscanner.net/images/airlines/small/49.png">
								</div>
								<div class="col s12 m9">
									<div style="display: flex;">
										<div style="padding: 0px 12px;"><span style="font-size: 1.4rem;">23:03</span><br>BLR</div>
										<div style="padding: 0px 12px; text-align: center;">
											<div class="flightDuration">2h 40</div>
											<div class="flightType">Direct</div>
										</div>
										<div style="padding: 0px 12px;"><span style="font-size: 1.4rem;">23:03</span><br>BLR</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="col s12 m4 p0 pointCursor">
						<!-- <div style="width: 14px;
					    height: 14px;
					    border-radius: 50%;
					    position: absolute;
					    top: -8px;
					    right: 244px;
					    background: white;
					    box-shadow: inset 0px -1px 0px 0px rgba(37,32,3,0.2)"></div>
						<div style="width: 14px;
					    height: 14px;
					    border-radius: 50%;
					    position: absolute;
					    right: 244.5px;
					    bottom: -10px;
					    background: white;
					    box-shadow: inset 0px 2px 0px 0px rgba(37,32,3,0.2)"></div> -->
						<div class="flightCardP2">
							<div class="ca7">5 deals from</div>
							<div class="flightPrice" style="padding-bottom:5px; ">&#x20b9; 10,582</div>
							<div class="flightPrice">&#x20b9; 10,582</div>
							<div class="btn">Select <i class="material-icons vaB">arrow_forward</i></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		$(document).ready(function(){
    		$('.collapsible').collapsible();
  		});

	</script>
	<!-- Compiled and minified JavaScript -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
</body>
</html>