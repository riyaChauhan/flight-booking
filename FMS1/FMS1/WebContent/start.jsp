<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
  <!DOCTYPE html>
  <html>

  <head>
  <meta charset="ISO-8859-1">
      <!--Import Google Icon Font-->
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      
      <!-- font -->
      <link href="https://fonts.googleapis.com/css?family=Nunito&display=swap" rel="stylesheet">



      <!--Import materialize.css-->
      <link type="text/css" rel="stylesheet" href="css/materialize.min.css" media="screen,projection" />
      <link rel="stylesheet" type="text/css" href="css/loginCSS.css">

      <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
      
      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <style type="text/css">

      </style>
      <title>Home</title>
     
  </head>
  <%
  if((String)session.getAttribute("status")==null)
  {
	  RequestDispatcher rd=request.getRequestDispatcher("index.jsp");
	  rd.forward(request, response);
  }
  %>

  <body>
<%@page import="java.util.ArrayList"%>      
<%@page import="com.wipro.fms.DAO.UserDao"%>
<%@page import="com.wipro.fms.bean.UserBean"%>
<%@page import="java.util.Iterator"%>
<style>body{font-family: 'Nunito' !important;} .bg_trans{background-color:transparent !important;} .bg021{background-color: #02122c !important;}</style> 
      <nav>
          <div class="nav-wrapper bg021">
              <div class="container">
                  <a href="#!" class="brand-logo" style="font-family: 'Pacifico', cursive;"><b>musafir.com</b></a>
              </div>
              <a href="#" data-target="side" class="sidenav-trigger  navy-color lighten-2"><b><i class="material-icons">menu</i></b></a>
              <ul class="right hide-on-med-and-down">
                  <li><a class="btn bg_trans  waves-effect "> <b>About</b></a></li>
                  <li><a class="dropdown-trigger btn  bg_trans  waves-effect " data-target="acc-drop"> <b>Account</b></a></li>
                
                  <li><a data-target="modal4" class=" btn bg_trans  waves-effect modal-trigger"> <b>LogOut</b></a></li>
                
                  
                  
              </ul>
          </div>
      </nav>


      <!-- sidenav -->



      <div class="sidenav" id="side">


          <div id="uli" class="container">
              <!-- <ul >
			<li style="">
				<a class="sidenav-close  tooltipped" data-position="right" data-tooltip="click to close"><i class="white-text material-icons">arrow_back</i></a>

			</li>
		</ul> -->

              <ul>
                  <li>
                      <a class="white-text sidenav-close">About</a>

                  </li>
                  <li>
                      <a class="dropdown-trigger btn  bg_trans  waves-effect " data-target="acc-drop"> <b>Account</b></a>
                  </li>
                  <li>
                      <a data-target="modal4" class=" btn bg_trans  waves-effect modal-trigger"> <b>LogOut</b></a>
                  </li>
                  <li>
                      <a class="white-text dropdown-trigger" data-target="acc-drop1">Account</a>
                  </li>
              </ul>
          </div>
          <footer id="sideFooter" class="footer-copyright ">
              <div>
                  <p>FOLLOW US</p>
                  <a class="sidenav-close">
                      <img src="icons/facebook.png">
                  </a>
                  <a class=" sidenav-close">
                      <img src="icons/twitter.png">
                  </a>
                  <a class=" sidenav-close">
                      <img src="icons/instagram.png">
                  </a>
                  <a class=" sidenav-close">
                      <img src="icons/google-plus.png">
                  </a>
                  <div class="grey-text">
                      <p>©2019 Copyright</p>

                  </div>
              </div>
          </footer>


      </div>

      <!-- Dropdown Structure -->
      <ul id='acc-drop1' class='dropdown-content'>
          <li><a href="#!">one</a></li>
          <li><a href="#!">two</a></li>
          <li class="divider" tabindex="-1"></li>
          <li><a href="#!">three</a></li>
          <li><a href="#!"><i class="material-icons">view_module</i>four</a></li>
          <li><a href="#!"><i class="material-icons">cloud</i>five</a></li>
      </ul>

      <!-- s i d e n a v e n d -->




      <!-- User l o g i n   f o r m s  -->


      <!-- Modal Structure -->
      <div id="modal1" class="modal">
          <div class="modal-content">
              <!-- LOGIN Page -->
         
              <div class="">
                  <div class="grey lighten-4 row" style="display: inline-block; padding: 30px 30px 0px 30px;">

                      <form class="col s12" action="UserLoginServlet" method="post" >
                          <div class='row'>
                              <div class='col s12'>
                              </div>
                          </div>

                          <div class="imgal">
                              <img class="responsive-img" style="width: 250px; " src="images/login_img.png" alt="login" />

                              <p style="text-align: center;font-size: 25px;"><b>User Log In</b></p>
                          </div>
                          
                          <div class='row'>
                              <div class='input-field col s12'>
                                  <input class='validate' type='email' name='email' id='email' />
                                  <label for='email'>Enter your email</label>
                              </div>
                          </div>

                          <div class='row'>
                              <div class='input-field col s12'>
                                  <input class='validate' type='password' name='password' id='password' />
                                  <label for='password'>Enter your password</label><br>

                              </div>

                              <label id="to2">
                                  <span><input type="checkbox" class="filled-in" onclick='showpassword()' /></span>&nbsp;
                                  <span>Show password</span>
                              </label>


                              <label style='float: right;'>
                                  <a data-target="modal2" class="modal-trigger modal-close" Style="color:#d81b60;">Forget Password?</a>
                              </label>
                          </div>

                          <br />

							

                          <div class='row' >
                          
                              <button type='submit' value="LOGIN" name='btn_login' class='col s4 offset-s4 btn btn-medium login-btn waves-effect indigo'>Login</button>
                            
                          </div>
                            
                          <div class='row' style="text-align: center;font-size: 13px;">
                              <em>New User?</em>
                              <a data-target="modal3" class="modal-trigger modal-close">Create account</a>
                          </div>
                      </form>
                  </div>
              </div>
              
          </div>
      </div>
<!-- Admin l o g i n   f o r m s  -->


      <!-- Modal Structure -->
      <div id="modal5" class="modal">
          <div class="modal-content">
              <!-- LOGIN Page -->
         
              <div class="">
                  <div class="grey lighten-4 row" style="display: inline-block; padding: 30px 30px 0px 30px;">

                      <form class="col s12" action="AdminLoginServlet" method="post" >
                          <div class='row'>
                              <div class='col s12'>
                              </div>
                          </div>

                          <div class="imgal">
                              <img class="responsive-img" style="width: 250px; " src="images/login_img.png" alt="login" />

                              <p style="text-align: center;font-size: 25px;"><b>Admin Log In</b></p>
                          </div>
                          
                          <div class='row'>
                              <div class='input-field col s12'>
                                  <input class='validate' type='email' name='email' id='email' />
                                  <label for='email'>Enter your email</label>
                              </div>
                          </div>

                          <div class='row'>
                              <div class='input-field col s12'>
                                  <input class='validate' type='password' name='password' id='password' />
                                  <label for='password'>Enter your password</label><br>

                              </div>

                              <label id="to2">
                                  <span><input type="checkbox" class="filled-in" onclick='showpassword()' /></span>&nbsp;
                                  <span>Show password</span>
                              </label>


                              <label style='float: right;'>
                                  <a data-target="modal2" class="modal-trigger modal-close" Style="color:#d81b60;">Forget Password?</a>
                              </label>
                          </div>

                          <br />

							

                          <div class='row' >
                          
                              <button type='submit' value="login" name='btn_login' class='col s4 offset-s4 btn btn-medium login-btn waves-effect indigo'>Login</button>
                            
                          </div>
                            
                          <div class='row' style="text-align: center;font-size: 13px;">
                              <em>New User?</em>
                              <a data-target="modal3" class="modal-trigger modal-close">Create account</a>
                          </div>
                      </form>
                  </div>
              </div>
              
          </div>
      </div>



      <!--  F O R G E T     P A S S W O R D -->



      <!-- Modal Structure -->
      <div id="modal2" class="modal">
          <div class="modal-content">
              <!-- ForgetPassword Page -->


              <div class="box1" style=" text-align: center;">

                  <div class="grey lighten-4 row" style="display: inline-block; padding: 20px;width:100%;">
                      <div class='row' style="text-align: left;">
                          <a data-target="modal1" class="modal-trigger modal-close circle" style="color: #02122C;"><i class="material-icons ">arrow_back</i></a>
                      </div>
                      <form class="col s12" method="post">
                          <div class='row'>
                              <div class='col s12'>
                              </div>
                          </div>

                          <div class="imgal1">
                              <img class="responsive-img" id="imgal2" src="images/lock1.png" alt="login" />

                              <p style="text-align: center;font-size: 25px;"><b>Forgot Password</b></p>
                          </div>
                          <!--<h3 class="blue-text" align="center"><b>Log In</b></h3>-->
                          <div class='row'>
                              <div class='input-field col s12'>
                                  <input class='validate' type='email' name='email' id='email' />
                                  <label for='email'>Enter your email</label>
                              </div>
                          </div>
                          <br />
                          <div class='row'>
                              <button type='submit' value="login" name='btn_login' class='col s6 offset-s3 btn btn-medium login-btn waves-effect indigo'>Send Link</button>
                              <br>
                          </div>

                      </form>
                  </div>
              </div>
          </div>
      </div>

      <!--  N E W    U S E R  -->

      <div id="modal3" class="modal">
          <div class="modal-content">
              <!-- LOGIN Page -->

              <div class="">
                  <div class="grey lighten-4 row" style="display: inline-block; padding: 0px 30px 0px 30px;">

                      <form class="col s12" action="UserLoginServlet" method="post" ">
                          <div class='row'>
                              <div class='col s12'>
                              </div>
                          </div>

                          <div class="imgal">
                              <img class="responsive-img" style="width: 250px; " src="images/signup.png" alt="login" />
                              <p style="text-align: center;font-size: 20px;"><b>Create Account</b></p>

                          </div>


                          <!--<h3 class="blue-text" align="center"><b>Log In</b></h3>-->


                          <div class='row'>
                              <div class='input-field col s6'>
                                  <input class='validate' name='firstname' id='first' type='text' />
                                  <label for='first'>First Name</label>

                              </div>
                              <div class='input-field col s6'>
                                  <input type='text' name='lastname' id='last'>
                                  <label for='last'>Last Name</label>
                              </div>
                          </div>

                          <div class='row'>
                              <div class='input-field col s12'>
                                  <input class='validate' type='text' name='phonenumber' id='phonenumber'>
                                  <label for='phonenumber'>Enter Your Phone Number</label>

                              </div>
                          </div>


                          <div class='row'>
                              <div class='input-field col s12'>
                                  <input class='validate' type='email' name='email' id='email' />
                                  <label for='email'>Enter your Email Id</label>
                              </div>

                          </div>

                          <div class='row'>
                              <div class='input-field col s11'>
                                  <input class='validate' type='password' name='password' id='password' />
                                  <label for='password'>Enter your password</label>
                              </div>
                              <i class="material-icons right eyesignup" style="margin-top: -33px;color: rgba(0,0,0,0.7);margin-right: 10px;cursor:pointer;" onclick="showpassword()">remove_red_eye</i>

                          </div>
                          <br> <%String s1=request.getParameter("status"); %>
                          <div class='row'>
                              <button type='submit' value="REGISTER" name='btn_login' class='col s4 offset-s4 btn btn-medium login-btn waves-effect indigo' onclick="M.toast({html: '<%=s1%>'})">Register</button>
                              
  

                          </div>
                          <div class='row' style="text-align: center;font-size: 13px;">
                              <em>Already have an account?</em>
                              <a data-target="modal1" class="modal-trigger modal-close">LogIn</a>
                          </div>

                      </form>
                  </div>
              </div>
          </div>
      </div>


  <!--  logout    P A S S W O R D -->



      <!-- Modal Structure -->
      <div id="modal4" class="modal">
          <div class="modal-content">
              <!-- ForgetPassword Page -->


              <div class="box1" style=" text-align: center;">

                  <div class="grey lighten-4 row" style="display: inline-block; padding: 20px;width:100%;">
                      <div class='row' style="text-align: left;">
                          <a data-target="start.jsp" class="modal-trigger modal-close circle" style="color: #02122C;"><i class="material-icons ">arrow_back</i></a>
                      </div>
                      <form class="col s12" action="UserLoginServlet"method="post">
                          <div class='row'>
                              <div class='col s12'>
                              </div>
                          </div>

                          <div class="imgal1">
                              <img class="responsive-img" id="imgal2" src="images/lock1.png" alt="login" />
                                
                              <p style="text-align: center;font-size: 20px;"><b>Confirm Logout!</b></p>
                          </div>
                          <!--<h3 class="blue-text" align="center"><b>Log In</b></h3>-->
                          </br>
                          
                          <div class='row'>
                              <button type='submit' value="logout" name='btn_login' class='col s4 offset-s4 btn btn-medium login-btn waves-effect indigo'>Yes</button>
                              <br>
                          </div>

                      </form>
                  </div>
              </div>
          </div>
      </div>

              

      <!-- s e a r c h   b a r -->


      <style type="text/css">
          body {
              overflow-x: hidden;
              font-family: 'Nunito';

          }
          

          .bookingCardBg {
              background-image: url('images/bookingCardImg1.jpg');
              background-color: #c4eaf3;
              background-position: center;
              background-repeat: no-repeat;
              background-size: cover;
          }

          .bookingCardContainer {
              width: 94%;
              margin: auto;
          }

          .bookingCardWrapper {
              padding: 60px 0px;
          }

          .bookingCardWrapper h1 {
              margin: 0;
              color: white;
              font-weight: 600;
              padding-top: 80px;
          }

          .bookingCardCont {
              background-color: #02122c;
              padding: 20px;
              margin: 14px 0px;
              border-radius: 4px;
              color: white;
          }

          .bookingCardCont form {
              padding: 10px 0px;
          }

          .bookingCardCont form col {
              padding: 0px !important;
          }

          .bookingCardContR1 {
              background: white;
              color: #0a0a58;
              border-radius: 2px;
              margin-bottom: 14px;
          }

          .bookingCardContR1>.col {
              border-right: 1px solid #b3b2b7;
          }

          .loopbdr {
              border-right: 1px solid #b2b3b7;
              border-left: 1px solid #b2b3b7;
              height: 100%;
          }

          .searchFlightBtn {
              margin: 14px 0px;
          }

          .searchFlightIcon {
              vertical-align: bottom;
          }

          [type="date"] {
              background: #fff url(https://cdn1.iconfinder.com/data/icons/cc_mono_icon_set/blacks/16x16/calendar_2.png) 97% 50% no-repeat;
          }

          [type="date"]::-webkit-inner-spin-button {
              display: none;
          }

          [type="date"]::-webkit-calendar-picker-indicator {
              opacity: 0;
              position: relative;
              left: 2px;
          }

          #travellerSelectorWrapper {
              display: inline;
              position: relative;
              bottom: 112px;
              z-index: 99;
          }

          .travellerSelectorCont {
              padding: 20px 0px !important;
              border-radius: 4px;
              position: absolute;
              top: 22px;
          }

          .travellerSelectorCont>span {
              font-weight: 700;
              color: #02122c;
              font-size: 1.2rem;
              line-height: 36px;
          }

          .travellerSelectorCont .right {
              font-weight: 700;
              cursor: default;
          }

          .findCardArrow {
              font-size: 100px;
              position: relative;
              bottom: 36px;
              color: white;
          }

          .passengerHeading {
              color: #111236;
              font-weight: bolder;
              letter-spacing: 1px;
          }

          .passengerNoIconCont {
              height: 30px;
              border: 2px solid #02122c;
              width: 30px;
              border-radius: 50%;
              text-align: center;
              display: inline-block;
              cursor: pointer;
              user-select: none;
              -webkit-user-select: none;
          }

          .passengerNoIconCont:hover {
              border: 3px solid #31aca8;
          }

          .passengerNoIcon {
              color: #31aca8;
              font-weight: 900;
              font-size: 24px;
          }

          .passengerinfo {
              padding-top: 10px !important;
          }

          .passengerinfo span {
              font-size: 1rem;
              letter-spacing: 2px;
              border-bottom: 1px solid #b2b2be;
              padding-right: 26px;
          }

          .passengerinfo1 {
              padding: 10px 6px;
              font-size: 0.9rem;
          }

          .passengerinfo2 {
              color: #111236;
              font-weight: bolder;
              letter-spacing: 1px;
              padding-bottom: 10px;
          }

          #noOfTickets {
              display: inline-block;
              vertical-align: bottom;
              font-size: 1.6rem;
              padding: 0px 10px;
              font-weight: 700;
          }

          .noOfPassengerText {
              display: inline-block;
              vertical-align: super;
              padding-left: 10px;
          }

          .conditions {
              font-size: 0.8rem;
              color: #928f96;
          }

          /*genral class*/
          .p020 {
              padding: 0px 20px !important;
          }

          .taC {
              text-align: center;
          }

          .taR {
              text-align: right;
          }

          .p0 {
              padding: 0px !important;
          }

          .taE {
              text-align: end !important;
          }

          .bg31a {
              background-color: #31aca8 !important;
          }
          
          .bg021{background-color: #02122c !important;}

          .m0 {
              margin-top: 0px !important;
          }

          .br8021c {
              border-right: 8px solid #02122c !important;
          }

          .p160 {
              padding: 16px 0px;
          }

          .curPoint {
              cursor: pointer !important;
          }

          /*overrided class*/
          input:not([type]):focus:not([readonly]),
          input[type=text]:not(.browser-default):focus:not([readonly]),
          input[type=password]:not(.browser-default):focus:not([readonly]),
          input[type=email]:not(.browser-default):focus:not([readonly]),
          input[type=url]:not(.browser-default):focus:not([readonly]),
          input[type=time]:not(.browser-default):focus:not([readonly]),
          input[type=date]:not(.browser-default):focus:not([readonly]),
          input[type=datetime]:not(.browser-default):focus:not([readonly]),
          input[type=datetime-local]:not(.browser-default):focus:not([readonly]),
          input[type=tel]:not(.browser-default):focus:not([readonly]),
          input[type=number]:not(.browser-default):focus:not([readonly]),
          input[type=search]:not(.browser-default):focus:not([readonly]),
          textarea.materialize-textarea:focus:not([readonly]) {
              border-bottom: none;
              box-shadow: none;
              -webkit-box-shadow: none;
          }

          input:not([type]),
          input[type=text]:not(.browser-default),
          input[type=password]:not(.browser-default),
          input[type=email]:not(.browser-default),
          input[type=url]:not(.browser-default),
          input[type=time]:not(.browser-default),
          input[type=date]:not(.browser-default),
          input[type=datetime]:not(.browser-default),
          input[type=datetime-local]:not(.browser-default),
          input[type=tel]:not(.browser-default),
          input[type=number]:not(.browser-default),
          input[type=search]:not(.browser-default),
          textarea.materialize-textarea {
              border-bottom: none;
              margin: 0px;
          }

          input[type=text]:not(.browser-default):disabled {
              color: inherit;
              border-bottom: none;
          }

          [type="radio"]:not(:checked)+span,
          [type="radio"]:checked+span {
              padding-left: 26px !important;
          }

          [type="radio"]:checked+span:after,
          [type="radio"].with-gap:checked+span:after {
              background-color: #31aca8;
              border: none !important;
          }

          [type="radio"]:checked+span:after,
          [type="radio"].with-gap:checked+span:before,
          [type="radio"].with-gap:checked+span:after {
              border: 2px solid white;
          }

          input[type=date]:not(.browser-default):disabled {
              border-bottom: none;
          }

      </style>


      <div>
          <div class="row">
              <div class="col s12 bookingCardBg p0">
                  <div class="bookingCardContainer">
                      <div class="col s12 bookingCardWrapper">
                          <h1>Where to next?</h1>
                          <div class="col s12 bookingCardCont p020">
                              <form>
                                  <!--radio button for single or round trip-->
                                  <div class="col s12 m12 l6 p0">
                                      <div class="col s6 m2 l3 p0">
                                          <p>
                                              <label>
                                                  <input class="with-gap" name="group1" type="radio" onclick="oneWay();" checked />
                                                  <span>One-Way</span>
                                              </label>
                                          </p>
                                      </div>
                                      <div class="col s6 m3 l3 p0">
                                          <p>
                                              <label>
                                                  <input class="with-gap" name="group1" type="radio" onclick="roundTrip();" />
                                                  <span>Round Trip</span>
                                              </label>
                                          </p>
                                      </div>
                                  </div>
                                  <!-- for desktop -->
                                  <div class="show-on-large hide-on-med-and-down">
                                      <!--heading for booking card columns-->
                                      <div class="col s12 p0">
                                          <div class="col s3 p0">From</div>
                                          <div class="col s2 p0">To</div>
                                          <div class="col s2 p0">Depart</div>
                                          <div class="col s2 p0">Return</div>
                                          <div class="col s3 p0">Cabin Class & Travellers</div>
                                      </div>

                                      <!--booking card columns-->
                                      <div class="col s12 bookingCardContR1 p0">
                                          <div class="col s3 p0">
                                              <div class="col s10"><input type="text" name="FromCity" placeholder="City"></div>
                                              <div class="col s2 taC"><i class="material-icons" style="padding-top: 10px !important;">loop</i></div>
                                          </div>
                                          <div class="col s2">
                                              <input type="text" name="DesCity" placeholder="City">
                                          </div>
                                          <div class="col s2">
                                              <input type="date" name="departDate" value="2019-12-21">
                                          </div>
                                          <div class="col s2">
                                              <input type="date" name="returnDate" id="returnDate" value="2019-12-27" disabled>
                                          </div>
                                          <div class="col s3 p0" onclick="myFunction()">
                                              <div class="col s10"><input type="text" name="traveller" placeholder="1 traveller, Economy" id="traveller" class="curPoint" readonly></div>
                                              <div class="col s2 taE p0"><i class="material-icons" style="padding-top: 10px !important;">arrow_drop_down</i></div>
                                          </div>
                                      </div>
                                  </div>

                                  <!-- for tablet -->
                                  <div class="show-on-medium hide-on-large-only hide-on-small-only">
                                      <!--heading for booking card columns part1-->
                                      <div class="col s12 p0">
                                          <div class="col s5 p0">From</div>
                                          <div class="col s1 p0"></div>
                                          <div class="col s6 p0">To</div>
                                      </div>

                                      <!--booking card columns part1-->
                                      <div class="col s12 bookingCardContR1 p0">
                                          <div class="col s5">
                                              <input type="text" name="FromCity" placeholder="City">
                                          </div>
                                          <div class="col s1 taC" style="height: 42px;">
                                              <i class="material-icons" style="padding-top: 10px !important;">loop</i>
                                          </div>
                                          <div class="col s6">
                                              <input type="text" name="DesCity" placeholder="City">
                                          </div>
                                      </div>

                                      <!--heading for booking card columns part2-->
                                      <div class="col s12 p0">
                                          <div class="col s3 p0">Depart</div>
                                          <div class="col s3 p0">Return</div>
                                          <div class="col s6 p0">Cabin Class & Travellers</div>
                                      </div>
                                      <!--booking card columns part2-->
                                      <div class="col s12 bookingCardContR1 p0">
                                          <div class="col s3 br8021c">
                                              <input type="date" name="departDate" value="2019-12-21">
                                          </div>
                                          <div class="col s3 br8021c">
                                              <input type="date" name="returnDate" id="returnDate" value="2019-12-27" disabled>
                                          </div>
                                          <div class="col s6 p0" onclick="myFunction()">
                                              <div class="col s10"><input type="text" class="curPoint" name="traveller" placeholder="1 traveller, Economy" id="traveller" placeholder="travellers" readonly></div>
                                              <div class="col s2 taE p0"><i class="material-icons" style="padding-top: 10px !important;">arrow_drop_down</i></div>
                                          </div>
                                      </div>
                                  </div>

                                  <!-- for mobile -->
                                  <div class="show-on-small hide-on-large-only hide-on-med-only">
                                      <!--heading for booking card columns part1-->
                                      <div class="col s12 p0">
                                          <div class="col s12 p0">From</div>
                                      </div>

                                      <!--booking card columns part1-->
                                      <div class="col s12 bookingCardContR1 p0">
                                          <div class="col s12">
                                              <input type="text" name="FromCity" placeholder="City">
                                          </div>
                                      </div>
                                      <!--heading for booking card columns part2-->
                                      <div class="col s12 p0">
                                          <div class="col s12 p0">To</div>
                                      </div>
                                      <!--booking card columns part2-->
                                      <div class="col s12 bookingCardContR1 p0">
                                          <div class="col s12">
                                              <input type="text" name="DesCity" placeholder="City">
                                          </div>
                                      </div>
                                      <!--heading for booking card columns part3-->
                                      <div class="col s12 p0">
                                          <div class="col s6 p0">Depart</div>
                                          <div class="col s6 p0">Return</div>
                                      </div>
                                      <!--booking card columns part3-->
                                      <div class="col s12 bookingCardContR1 p0">
                                          <div class="col s6 br8021c">
                                              <input type="date" name="departDate" value="2019-12-21">
                                          </div>
                                          <div class="col s6">
                                              <input type="date" name="returnDate" id="returnDate" value="2019-12-27" disabled>
                                          </div>
                                      </div>
                                      <!--heading for booking card columns part4-->
                                      <div class="col s12 p0">
                                          <div class="col s12 p0">Cabin Class & Travellers</div>
                                      </div>
                                      <!--booking card columns part4-->
                                      <a href="#travellerSelectorWrapper">
                                          <div class="col s12 bookingCardContR1 p0" onclick="myFunction();">
                                              <div class="col s12 p0">
                                                  <div class="col s10"><input class="curPoint" type="text" name="traveller" placeholder="1 traveller, Economy" id="traveller" readonly></div>
                                                  <div class="col s2 taE p0"><i class="material-icons" style="padding-top: 10px !important;">arrow_drop_down</i></div>
                                              </div>
                                          </div>
                                      </a>
                                  </div>

                                  <!-- search flight button -->
                                  <div class="col s12 searchFlightBtn taR p0">
                                      <div class="col s4 offset-s8 p0">
                                          <a class="waves-effect waves-light btn bg31a">Search Flights <i class="material-icons searchFlightIcon">arrow_forward</i></a>
                                      </div>
                                  </div>
                              </form>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <div class="row" id="travellerSelectorWrapper" style="display: none;">
              <div class="col s12">
                  <div class="col s12 m4 offset-m7 taC" style="height: 22px;"><i class="material-icons findCardArrow cardArrow">arrow_drop_up</i></div>
                  <div class="card col s12 m4 offset-m7 travellerSelectorCont m0 curPointu">
                      <div class="col s12 ">
                          <div class="col s8 left-align p0 passengerHeading">Cabin Class & Travellers</div>
                          <div class="col s4 right-align p0 curPoint" onclick=" myFunction();">Done</div>
                      </div>
                      <div class="col s12 passengerinfo">
                          <span>
                              Cabin class
                          </span>
                          <div class="passengerinfo1"><b>We can show only Economy prices for this search.</b>
                              <br>
                              To see Business and First Class options, please tell us your exact dates and/or destination city.
                          </div>
                          <div class="selectTicketSection">
                              <div class="passengerinfo2">Passengers</div>
                              <div id="decrease" class="passengerNoIconCont" onclick="decrease();"><i class="material-icons passengerNoIcon">expand_more</i></div>
                              <div id="noOfTickets">1</div>
                              <div id="increase" class="passengerNoIconCont" onclick="increase();"><i class="material-icons passengerNoIcon">expand_less</i></div>
                              <div class="noOfPassengerText">no. of passengers</div>
                          </div>
                          <div class="conditions">
                              <div class="p160">
                                  Your age at time of travel must be valid for the age category booked. Airlines have restrictions on under 18s travelling alone.
                              </div>
                              <div>
                                  Age limits and policies for travelling with children may vary so please check with the airline before booking.
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <script type="text/javascript">
              function myFunction() {
                  var travlSel = document.getElementById("travellerSelectorWrapper");
                  if (travlSel.style.display === "none") {
                      travlSel.style.display = "block";
                  } else {
                      travlSel.style.display = "none";
                  }
              }

              function oneWay() {
                  document.getElementById("returnDate").setAttribute("disabled", "true");
              }

              function roundTrip() {
                  document.getElementById("returnDate").removeAttribute("disabled");
              }
              var counter = 1;

              function increase() {
                  if (counter < 10) {
                      counter++;
                  }
                  document.getElementById('noOfTickets').innerHTML = counter;
                  var text = counter > 1 ? " travellers" : " traveller";
                  document.getElementById('traveller').value = counter + text + ", Economy";

              }

              function decrease() {
                  if (counter > 0) {
                      counter--;
                  }
                  document.getElementById('noOfTickets').innerHTML = counter;
                  var text = counter > 1 ? " travellers" : " traveller";
                  document.getElementById('traveller').value = counter + text + ", Economy";

              }

          </script>
      </div>

      <!-- s e a r c h   b a r -->


      <script>
          function showpassword() {
              var x = document.getElementById("password");
              if (x.type === 'password') {
                  x.type = 'text';
              } else {
                  x.type = 'password';
              }

          }

      </script>



      <!-- jquery initialization -->
      <script type="text/javascript">
          $(document).ready(function() {
              $('.modal').modal();
          });

      </script>
      <!-- l o g i n   f o r m s     E N D S  -->
      
      <!--JavaScript at end of body for optimized loading-->
      <script type="text/javascript" src="js/materialize.min.js"></script>
      <script type="text/javascript">
          $(document).ready(function() {
              $('.sidenav').sidenav();
          });

          $('.dropdown-trigger').dropdown();

          $(document).ready(function() {
              $('.modal').modal();
          });
          // login show pass
          $(".toggle-password").click(function() {

              $(this).toggleClass("");
              var input = $($(this).attr("toggle"));
              if (input.attr("type") == "password") {
                  input.attr("type", "text");
              } else {
                  input.attr("type", "password");
              }
          });
          $(document).ready(function() {
              $('.tooltipped').tooltip();
          });

      </script>
  

  <br>

  <!-- t r a v e l p r o f i l e-->

  <style>
      @charset "ISO-8859-1";


	  .recom_img{overflow: hidden;}
      .card .recom_img img{transition: transform 1s;}

      .card .recom_img img:hover {
         transform: scale(1.2);
      }


      .card-title i b {
          font-weight: 600;
          font-size: 1em;
      }

      //media query
      @media only screen and (min-width: 993px) {
          .container {
              width: 80% !important;
          }
      }
      trapad{
          padding: 10px;
      }
      
      .pageSmallHeading{
	    font-weight: 700;
		font-size: 1.75rem;
		line-height: 1.875rem;
		color: #111236;
		padding: 0.75rem;
		margin: 0;
	 }
	 
	 .recom_content{
	 	background-color: #02122c !important;
    	padding: 16px 24px !important;
	 }
	 
	 .recom_content p{
	 	color:white;
	 	font-size:1.5rem;
	 	font-weight: 600;
	 }
	 .recom_content a{
	 	line-height: 1.5rem;
	    letter-spacing: normal;
	    font-weight: 700;
	    color: #FF7B59;
	 }
	 .recom_content a i{
	 	vertical-align: bottom !important;
	 }

  </style>
  <div class="container">
      <div class="row">
      <h2 class="pageSmallHeading">Recommendations for you</h2>
          <!-- CARD 1 -->
          <div class="col s12 m4">
              <div class="card">
                  <div class="card-image recom_img">
                      <img src="images/delhi.jpg" width="200px" height="250px">
                      
                  </div>
                  <div class="card-content recom_content">
                      <p>Delhi, India</p>
                      <a>Get More Info <i class="material-icons">keyboard_arrow_right</i></a></a>
                  </div>
                 
              </div>
          </div>
          <!-- CARD 2 -->
          <div class="col s6 m4">
              <div class="card">
                  <div class="card-image recom_img">
                      <img src="images/Bengaluru.jpeg" width="200px" height="250px">
                      
                  </div>
                  <div class="card-content recom_content">
                     <p>Bengaluru, India</p>
                     <a>Get More Info <i class="material-icons">keyboard_arrow_right</i></a>
                  </div>
                  
              </div>
          </div>
          <!-- CARD 3-->
          <div class="col s6 m4">
              <div class="card">
                  <div class="card-image recom_img">
                      <img src="images/mumbai.jpg" width="200px" height="250px">
                      
                  </div>

                  <div class="card-content recom_content">
                      <p>Mumbai, India</p>
                      <a>Get More Info <i class="material-icons">keyboard_arrow_right</i></a></a>
                  </div>
                  
              </div>
          </div>
      </div>
  </div>
  <!-- t r a v e l p r o f i l e-->









  <!--    b l o g   1-->
  <style>
      @charset "ISO-8859-1";

      .pt20{padding-top:20px;}

      //media query
      @media only screen and (min-width: 993px) {
          .container {
              width: 80% !important;
          }
      }

      @media only screen and (min-width: 601px) {
          .container {
              width: 90% !important;
          }
      }

      @media only screen and (min-width: 550px) {
          .container {
              width: 100% !important;
          }
      }

  </style>
  <div class="container" style="align-content:center;">

	  <h2 class="pageSmallHeading pt20">Travel with us to become a Musafir</h2>
		<div class="row" style="padding-top: 100px;">
			<!-- CARD 1 -->
			<div class="col s12 m4">
				<div class="card"
					style="transform: skewY(-14deg); height: auto !important; padding-bottom: 20px; box-shadow: rgba(0, 0, 0, 0.3) !important;">
					<div class="card-image"
						style="transform: skewY(14deg); width: 100%; height: 190px; margin: auto;">
						<img src="images/blogimg1.svg" style="height: 260px; top: -70px;">

					</div>
					<div class="span"
						style="height: auto; transform: skewY(14deg); padding-left: 20px;">
						<span class="card-title black-text"><i><b>Go
									anywhere</b></i></span>
					</div>
					<div class="card-content"
						style="transform: skewY(14deg); padding-left: 20px;">
						<p>It's your world and we'll help you explore it. Find the
							best prices across millions of flights, hotels and car hire
							options to create your perfect trip.</p>
					</div>
				</div>
			</div>
			<!-- CARD 2 -->
			<div class="col s12 m4">
				<div class="card"
					style="transform: skewY(-14deg); transform-origin: bottom; height: auto !important; padding-bottom: 30px; box-shadow: rgba(0, 0, 0, 0.3);">
					<div class="card-image"
						style="transform: skewY(14deg); width: 100%; height: 190px; margin: auto;">
						<img src="images/blogimg2.svg"
							style="height: 260px; top: -70px !important;">
					</div>
					<div class="span"
						style="height: auto; transform: skewY(14deg); padding-left: 20px;">
						<span class="card-title black-text"><i><b>Keep it
									simple</b></i></span>
					</div>
					<div class="card-content"
						style="transform: skewY(14deg); padding-left: 20px;">
						<p>No hidden fees. No hidden charges. No funny business. With
							us, you'll always know exactly where your money goes. So you can
							relax before your trip even begins.</p>
					</div>
				</div>
			</div>
			<!-- CARD 3 -->
			<div class="col s12 m4">
				<div class="card"
					style="transform: skewY(-14deg); transform-origin: bottom; height: auto !important; padding-bottom: 30px; box-shadow: rgba(0, 0, 0, 0.3);">
					<div class="card-image"
						style="transform: skewY(14deg); width: 80%; height: 190px; margin: auto;">
						<img src="images/blogimg3.svg"
							style="height: 320px; top: -130px !important;">

					</div>
					<div class="span"
						style="height: auto; transform: skewY(14deg); padding-left: 20px;">
						<span class="card-title black-text"><i><b>Travel
									your way</b></i></span>
					</div>
					<div class="card-content"
						style="transform: skewY(14deg); padding-left: 20px;">
						<p>Know where you want to go? See the best time to book.
							Feeling flexible? Get deals on everything from quick breaks to
							epic adventures.</p>
					</div>
				</div>
			</div>
		</div>
	</div>





  <!--    b l o g   1-->



  <!--f o o t e r-->
  <link rel="stylesheet" href="css/footerstyle.css">

  <footer class="footer-distributed" style="margin-top: 10%">

      <div class="footer-left">

          <h3><a href="#!" class="brand-logo white-text" style="font-family: 'Pacifico', cursive;"><b>musafir.com</b></a></h3>

          <p class="footer-links">
              <a href="#" class="link-1">Home</a>

              <a href="#">About</a>

              <a href="#">FAQ</a>
          </p>

          <p class="footer-company-name"><i>Musafir <i class="tiny material-icons">copyright</i> 2019</i></p>
      </div>

      <div class="footer-center">

          <div>
              <i class="medium material-icons">location_on</i>
              <p><span>MUSAFIR Limited</span>Electronic city, bengaluru-560100</p>
          </div>

          <div>
              <i class="medium material-icons">call</i>
              <p>+91-7987371529</p>
          </div>

          <div>
              <i class="medium material-icons">mail</i>
              <p><a href="mailto:support@musafir.com">support@musafir.com</a></p>
          </div>

      </div>

      <div class="footer-right">

          <p class="footer-company-about">
              <span>About the company</span>
              Lorem ipsum dolor sit amet, consectateur adispicing elit. Fusce euismod convallis velit, eu auctor lacus vehicula sit amet.
          </p>

          <div class="footer-icons">

              <a href="#"><i class="fa fa-facebook"><img src="icons/facebook-3-32.png"></i></a>
              <a href="#"><i class="fa fa-twitter"><img src="icons/twitter-3-32.png"></i></a>
              <a href="#"><i class="fa fa-linkedin"><img src="icons/linkedin-3-32.png"></i></a>
              <a href="#"><i class="fa fa-github"><img src="icons/github-3-32.png"></i></a>

          </div>

      </div>

  </footer>
  
	 
 

  <!--f o o t e r-->
  
  

      </body>
    
  </html>


