package com.wipro.fms.bean;

import java.sql.Date;

public class TicketBean {
	private String ticketID;
	 private String passengerID;
	 private Long flightId;
	 private String depTime; // for Departure time
	 private String arvTime; // for Arrival time
	 private String status;
	 private String source;
	 private String dest; //for destination
	 private String phNo; //for phone no.
	 private Date bookDate;
	 private String travelDate;
	 private int totalPrice;
	 
	public String getTicketID() {
		return ticketID;
	}
	public void setTicketID(String ticketID) {
		this.ticketID = ticketID;
	}
	public String getPassengerID() {
		return passengerID;
	}
	public void setPassengerID(String passengerID) {
		this.passengerID = passengerID;
	}
	public Long getFlightId() {
		return flightId;
	}
	public void setFlightId(Long flightId) {
		this.flightId = flightId;
	}
	public String getDepTime() {
		return depTime;
	}
	public void setDepTime(String depTime) {
		this.depTime = depTime;
	}
	public String getArvTime() {
		return arvTime;
	}
	public void setArvTime(String arvTime) {
		this.arvTime = arvTime;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getDest() {
		return dest;
	}
	public void setDest(String dest) {
		this.dest = dest;
	}
	public String getPhNo() {
		return phNo;
	}
	public void setPhNo(String phNo) {
		this.phNo = phNo;
	}
	public Date getBookDate() {
		return bookDate;
	}
	public void setBookDate(Date bookDate) {
		this.bookDate = bookDate;
	}
	public String getTravelDate() {
		return travelDate;
	}
	public void setTravelDate(String travelDate) {
		this.travelDate = travelDate;
	}
	public int getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(int totPrice) {
		this.totalPrice = totPrice;
	}
	 
}
