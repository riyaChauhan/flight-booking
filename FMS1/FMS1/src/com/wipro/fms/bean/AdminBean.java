package com.wipro.fms.bean;

public class AdminBean {
private String firstName;
private String lastName;
private String email;
private long phoneNo;
private String password;
public String getFirstName() {
	return firstName;
}
public void setFirstName(String firstName) {
	this.firstName = firstName;
}
public String getLastName() {
	return lastName;
}
public void setLastName(String lastName) {
	this.lastName = lastName;
}
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}
public long getPhoneNo() {
	return phoneNo;
}
public void setPhoneNo(long phoneNo) {
	this.phoneNo = phoneNo;
}
public String getPassword() {
	return password;
}
public void setPassword(String password) {
	this.password = password;
}


}
