package com.wipro.fms.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.wipro.fms.DBUtil.DBUtil;
import com.wipro.fms.bean.UserBean;

public class UserDao {
	Connection con=null;
	ResultSet rs=null;
    PreparedStatement ps=null;
     public String isUser(String email,String password)
     {
    	 String status="";
    	
    	 try { con=DBUtil.getDBConnection();
    	 System.out.println(con);
    	      String sql="select * from userlogin where email=? and password=?";
    	      ps=con.prepareStatement(sql);
    	      ps.setString(1, email);
    	      ps.setString(2, password);
    	      rs=ps.executeQuery();
    	      if(rs.next())
    	      {  
    	    	    status ="yes";
    	      }
    	      else
    	      {  status ="no";}
    	 }
    	 catch(Exception e)
    	 {
    		 e.printStackTrace();
    	 }
    	      
    	 
    	 return status;
     }
     
     public String createUser(UserBean newUserBean) {
    	 int a=0;
  		String status="";
  		String lowerCaseEmail=newUserBean.getEmail();
  		lowerCaseEmail=lowerCaseEmail.toLowerCase();
  				
  		try {
  			con=DBUtil.getDBConnection();
  			String sql1="select * from userlogin where email=?";
  			ps=con.prepareStatement(sql1);
  			ps.setString(1,lowerCaseEmail);
  			rs=ps.executeQuery();
  			if(rs.next())
  			{
  			status="User Already Exist!";	
  			}
  			else
  			{
  			String sql="insert into userlogin values (?,?,?,?,?)";
  			ps=con.prepareStatement(sql);
  			ps.setString(1, newUserBean.getFirstName());
  			ps.setString(2, newUserBean.getLastName());
  			ps.setString(3,lowerCaseEmail );
  			ps.setString(4,newUserBean.getPhoneNo());
  			ps.setString(5, newUserBean.getPassword());
  			a=ps.executeUpdate();
  			if(a>=1)
  			{
  				status="Account Created!";
  			}
  			else
  			{
  				status="Sorry Account Not Created!";
  			}
  		  }
  		}
  		catch(Exception e)
  		{
  			status="DAO Error";
  		}
  		
  		
  		return status;
  	}	
 	
}
