package com.wipro.fms.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.wipro.fms.DBUtil.DBUtil;
import com.wipro.fms.bean.AdminBean;

public class AdminDao {
	Connection con=null;
	ResultSet rs=null;
    PreparedStatement ps=null;
     public String isAdmin(String email,String password)
     {
    	 String status="no";
    	 try { con=DBUtil.getDBConnection();
    	       String sql="select password from AdminLogin_tbl where email=?";
    		   ps=con.prepareStatement(sql);
    		   ps.setString(1,email );
    		   rs=ps.executeQuery();
    		   if(rs.next())
    		   {
    			   if(password.equals(rs.getString(1)))
    			   {
    				   status="yes";
    			   }
    			   else
    			   {
    				   status="no";
    			   }
    		   }
    		   con.close();ps.close();rs.close();
    	     }
    	 catch(Exception e)
        	 {
    		     status="Error"; 
    	     }
    	 return status;
     }
     
     public String CreateAdminUser(AdminBean newAdminBean) {
 		int a=0;
 		String status="";
 		String lowerCaseEmail=newAdminBean.getEmail();
 		lowerCaseEmail=lowerCaseEmail.toLowerCase();
 		try {
 			con=DBUtil.getDBConnection();
 			String sql="insert into adminlogin_tbl values (?,?,?,?,?)";
 			ps=con.prepareStatement(sql);
 			ps.setString(1, newAdminBean.getFirstName());
 			ps.setString(2, newAdminBean.getLastName());
 			ps.setString(3,lowerCaseEmail );
 			ps.setLong(4,newAdminBean.getPhoneNo());
 			ps.setString(5, newAdminBean.getPassword());
 			a=ps.executeUpdate();
 			if(a>=1)
 			{
 				status="Success";
 			}
 			else
 			{
 				status="Fail";
 			}
 		}
 		catch(Exception e)
 		{
 		  status="server error";
 		}
 		
 		return status;
 	}
}
