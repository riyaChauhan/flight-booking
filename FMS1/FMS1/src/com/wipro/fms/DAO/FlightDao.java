package com.wipro.fms.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import com.wipro.fms.bean.FlightBean;

public class FlightDao {
	Connection con=null;
	ResultSet rs=null;
    PreparedStatement ps=null;
    public ArrayList searchOneWayFlight(String source,String destination)
    {   ArrayList<FlightBean> list=new ArrayList<FlightBean>();
        try {
        	  String sql="select * from Flight_tbl where Source=? and Destination=?";
        	  ps=con.prepareStatement(sql);
        	  ps.setString(1, source);
        	  ps.setString(2, destination);
        	  rs=ps.executeQuery();
        	  while(rs.next())
        	  {
        		  FlightBean bean=new FlightBean();
        		  bean.setFlightId(rs.getLong("flightid"));
        		  bean.setSource(rs.getString("source"));
        		  bean.setDestination(rs.getString("destination"));
        		  bean.setDepTime(rs.getString("depTime"));
        		  bean.setArrTime(rs.getString("arrTime"));
        		  bean.setDuration(rs.getString("duration"));
        		  bean.setPrice(rs.getLong("price"));
        		  bean.setFlightName(rs.getString("flightName"));
                  list.add(bean);        		   	  
               }
            }
        catch(Exception e)
        {
        	e.printStackTrace();
        }
    	return list;
    }
    
    public ArrayList searchTwoWayFlight(String source,String destination)
    {   ArrayList<FlightBean> list=new ArrayList<FlightBean>();
        try {
        	  String sql="select * from Flight_tbl where Source=? and Destination=?";
        	  ps=con.prepareStatement(sql);
        	  ps.setString(1, source);
        	  ps.setString(2, destination);
        	  rs=ps.executeQuery();
        	  while(rs.next())
        	  {
        		  FlightBean bean=new FlightBean();
        		  bean.setFlightId(rs.getLong("flightid"));
        		  bean.setSource(rs.getString("source"));
        		  bean.setDestination(rs.getString("destination"));
        		  bean.setDepTime(rs.getString("depTime"));
        		  bean.setArrTime(rs.getString("arrTime"));
        		  bean.setDuration(rs.getString("duration"));
        		  bean.setPrice(rs.getLong("price"));
        		  bean.setFlightName(rs.getString("flightName"));
                  list.add(bean);        		   	  
               }
        	  
        	  ps=con.prepareStatement(sql);
        	  ps.setString(1, destination);
        	  ps.setString(2, source);
        	  rs=ps.executeQuery();
        	  while(rs.next())
        	  {
        		  FlightBean bean=new FlightBean();
        		  bean.setFlightId(rs.getLong("flightid"));
        		  bean.setSource(rs.getString("source"));
        		  bean.setDestination(rs.getString("destination"));
        		  bean.setDepTime(rs.getString("depTime"));
        		  bean.setArrTime(rs.getString("arrTime"));
        		  bean.setDuration(rs.getString("duration"));
        		  bean.setPrice(rs.getLong("price"));
        		  bean.setFlightName(rs.getString("flightName"));
                  list.add(bean);        		   	  
               }
            }
        catch(Exception e)
        {
        	e.printStackTrace();
        }
    	return list;
    }
}
