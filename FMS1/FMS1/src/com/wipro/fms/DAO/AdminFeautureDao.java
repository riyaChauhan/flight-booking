package com.wipro.fms.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.wipro.fms.bean.FlightBean;

public class AdminFeautureDao {
	Connection con=null;
	ResultSet rs=null;
    PreparedStatement ps=null;
    AdminFeautureDao dao=new AdminFeautureDao();
    public String addFlight(FlightBean bean){
    	String status="";
		try{   
			long flightId=dao.flightIdGenerate();
			bean.setFlightId(flightId);
			String sql="insert into Flight_tbl values (?,?,?,?,?,?,?,?)";
			ps=con.prepareStatement(sql);
			ps.setLong(1, bean.getFlightId());
			ps.setString(2, bean.getSource());
			ps.setString(3, bean.getDestination());
			ps.setString(4, bean.getDepTime());
			ps.setString(5, bean.getArrTime());
			ps.setString(6, bean.getDuration());
			ps.setLong(7, bean.getPrice());
			ps.setString(8, bean.getFlightName());
		
			status=(ps.executeUpdate()>0) ?"New Flight Added!" :"Flight not Added!" ;
		
		   }
		catch (Exception e) 
			{e.printStackTrace();}
		return status;
	}
    
    
    public long flightIdGenerate()
    {
    	long id=0;
    	try {
    		 String sql="select flight_seq.nextval from dual";
    		 ps=con.prepareStatement(sql);
    		 rs=ps.executeQuery();
    		 if(rs.next())
    		 {
    			 id=rs.getLong(1);
    		 }
            }
    	catch(Exception e)
    	{
    		e.printStackTrace();
    	}
    	return id;
    }
    
    public String updateFlight(FlightBean bean ,long flightId )
    {  String status="";
        try {
        	String sql="update flight_tbl set SOURCE=?,DESTINATION=?,DEPTIME=?,ARRTIME=?,DURATION=?,PRICE=?,FLIGHTNAME=? where flightid=?";
        	ps=con.prepareStatement(sql);
        	ps.setString(1,bean.getSource());
        	ps.setString(2, bean.getDestination());
        	ps.setString(3, bean.getDepTime());
        	ps.setString(4, bean.getArrTime());
        	ps.setString(5, bean.getDuration());
        	ps.setLong(6, bean.getPrice());
        	ps.setString(7, bean.getFlightName());
        	int n=ps.executeUpdate();
        	if(n>=1)
        	{
        		status="updated!";
        	}
        	else
        	{
        		status="not updated!";
        	}
        }
        catch(Exception e)
        {
        	e.printStackTrace();
        }
    	return status;
    
    }
    public String deleteFlight(long flightId)
    {   String status="";
        try {
        	String sql="delete * from flight_tbl where flightId=?";
        	ps=con.prepareStatement(sql);
        	ps.setLong(1, flightId);
            int n=ps.executeUpdate();
            if(n>=1)
            {
            	status="flight deleted!";
            }
            else
            {
            	status="flight not deleted!";
            }
            }
        catch(Exception e)
        {
        	e.printStackTrace();
        }
        
    	return status;
    }
    
    public FlightBean searchFlight(long flightId)
    {   FlightBean bean=new FlightBean();
        try {
	        	String sql="select * from flight_tbl where flightId=?";
	        	ps=con.prepareStatement(sql);
	        	ps.setLong(1, flightId);
	            rs=ps.executeQuery();
	            if(rs.next())
	            {  bean.setFlightName(rs.getString("firstName"));
	               bean.setSource(rs.getString("source"));
	               bean.setDestination(rs.getString("destination"));
	               bean.setArrTime(rs.getString("arrTime"));
	               bean.setDepTime(rs.getString("depTime"));
	               bean.setDuration(rs.getString("duration"));
	               bean.setPrice(rs.getLong("price"));	
	            }
	            else
	            {
	            	bean=null;
	            }
            }
        catch(Exception e)
        {
        	e.printStackTrace();
        }
        
    	return bean;
    }
}
