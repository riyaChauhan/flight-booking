package com.wipro.fms.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.wipro.fms.DAO.UserDao;
import com.wipro.fms.bean.UserBean;

/**
 * Servlet implementation class UserLoginServlet
 */
public class UserLoginServlet extends HttpServlet {
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String trigger=request.getParameter("btn_login");
		UserDao dao=new UserDao();
		 HttpSession session=request.getSession();
			if(trigger.equals("LOGIN")) {
				String email=request.getParameter("email");
				String password=request.getParameter("password");   
				String s=dao.isUser(email, password);
		             
		    		if(s.equals("yes"))
		    		{      
		    			 
		    			session.setAttribute("status", email);
		    			response.sendRedirect("start.jsp");
		    	
		    		}
		    		else
		    		{   
		  
		    			response.sendRedirect("index.jsp");
		    		}
			}
			else if(trigger.equals("logout"))
			{
				session.removeAttribute("status");
				//session.invalidate();
				response.sendRedirect("index.jsp");
			}
			else if(trigger.equals("REGISTER")) {
				UserBean bean=new UserBean();
				String firstName=request.getParameter("firstname");
				String lastName=request.getParameter("lastname");  
				String phoneNumber=request.getParameter("phonenumber");
				String email=request.getParameter("email");
				String password=request.getParameter("password");  
				bean.setFirstName(firstName);
				bean.setLastName(lastName);
				bean.setPhoneNo(phoneNumber);
				bean.setEmail(email);
				bean.setPassword(password);
				
				String s=dao.createUser(bean);
				if(s.equals("Account Created!"))
				{
					response.sendRedirect("index.jsp?status="+s);
				}
				else if(s.equals("Sorry Account Not Created!"))
				{
					response.sendRedirect("index.jsp?status="+s);
				}
				else if(s.equals("User Already Exist!"))
				{
					response.sendRedirect("index.jsp?status="+s);
				}
			}
			else if(trigger.equals("forget"))
			{
				
			}
			
			/*String trigger1=request.getParameter("button");
			UserDao dao1=new UserDao();
			 //HttpSession session=request.getSession();
				if(trigger1.equals("login")) {
					String email1 = request.getParameter("email");
					String password1 = request.getParameter("password");
					System.out.println(email1 + " " + password1);
					String s1=dao1.isUser(email1, password1);
			             
			    		if(s1.equals("yes"))
			    		{      
			    	
			    			//session.setAttribute("status", "Welcome");
			    			response.sendRedirect("start.jsp");
			    	
			    		}
			    		else
			    		{   
			    			response.sendRedirect("index.jsp");
			    		}
				}*/
			
	   	
	}

}
